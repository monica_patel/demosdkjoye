package com.sixdegrees.joyesdk

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import cc.cloudist.acplibrary.ACProgressConstant

import cc.cloudist.acplibrary.ACProgressFlower
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase



/**
 * Created by AhmedEltaher
 */


abstract class BaseActivity : AppCompatActivity() {


    var dialog: ACProgressFlower?=null
    private lateinit var firebaseAuth: FirebaseAuth
    lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        FirebaseApp.initializeApp(this)
        super.onCreate(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()
        auth = Firebase.auth

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }


    open fun showProgressDialog(statusType: Boolean) {

        if (dialog == null) {
            dialog = ACProgressFlower.Builder(this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY)
                    .build()
        }
        try {
            if (statusType) {
                if (!dialog!!.isShowing) {
                    dialog!!.show()
                }
            } else {
                if (dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
            }
        } catch (e: Exception) {
            Log.e("exception", e.toString())
        }
    }
}
