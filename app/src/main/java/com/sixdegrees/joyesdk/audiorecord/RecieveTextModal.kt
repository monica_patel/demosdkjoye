package com.sixdegrees.joyesdk.audiorecord

data class RecieveTextModal(
        var is_abusive: String="",
        var is_last_message: String="",
        var show_pie_graph_url: Any? = null,
        var delay: Any? = null,
        var data: DataModal = DataModal(),
        var failed: Boolean = false,
        var from: Any? = null,
        var layout: Any? = null,
        var message_read: Any? = null,
        var resolved: Boolean = false,
        var text: Any? = null,
        var timestamp: Long = 0,
        var type: Any? = null

)