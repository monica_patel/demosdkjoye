package com.sixdegrees.joyesdk.audiorecord

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VerificationCode (
        @Json(name = "varificationCode")
        var varificationCode: String
)