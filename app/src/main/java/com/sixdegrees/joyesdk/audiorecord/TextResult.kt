package com.sixdegrees.joyesdk.audiorecord

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TextResult (
    @Json(name = "alternatives")
    var alternatives: List<Alternative>

)