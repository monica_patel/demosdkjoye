package com.sixdegrees.joyesdk.audiorecord

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CloudToken (
    @Json(name = "accessToken")
    var accessToken: String

)