package com.sixdegrees.joyesdk.audiorecord

data class TextModal(
        var data: DataModal = DataModal(),
        var failed:Boolean=false,
        var from:String="",
        var layout:String="",
        var message_read:String="",
        var resolved:Boolean=false,
        var text:String="",
        var timestamp:Long=0,
        var type:String=""

)