package com.sixdegrees.joyesdk.audiorecord

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass




@JsonClass(generateAdapter = true)
data class SpeechToText (
        @Json(name = "results")
        var results: List<TextResult>
)