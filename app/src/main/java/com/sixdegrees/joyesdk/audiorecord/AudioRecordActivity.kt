package com.sixdegrees.joyesdk.audiorecord

import android.Manifest
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.view.animation.LinearInterpolator
import android.webkit.*
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatButton
import androidx.core.view.isVisible
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.google.firebase.storage.ktx.storageMetadata
import com.joye.sixdegreesit.R
import com.sixdegrees.joyesdk.BaseActivity

import com.sixdegrees.joyesdk.utils.Constants
import com.sixdegrees.joyesdk.utils.Network
import com.sixdegrees.joyesdk.utils.SharedPreference
import com.sixdegrees.joyesdk.utils.ApiManager

import kotlinx.android.synthetic.main.activity_audio_record.*
import kotlinx.android.synthetic.main.audio_layout.*
import kotlinx.android.synthetic.main.firebasehandle_layout.*
import kotlinx.android.synthetic.main.recording_layout.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class AudioRecordActivity : BaseActivity(), GetCustomToken, DownloadListener {

    val MY_PERMISSIONS_REQUESINT = 123
    val MAX_LENGTH = 20
    private var state: Boolean = false
    private var recorder: MediaRecorder? = null
    private var isFirstCall: Boolean = false
    private var mWebview: WebView? = null
    var token: String? = null
    var fileName: String? = null
    var tnc_url: String = ""
    var privacy_url: String = ""
    var faq_url: String = ""
    var userid: String = ""
    private var firebaseTextList: ArrayList<RecieveTextModal>? = null

    var stopped = false
    lateinit var timer: Timer





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_record)
        if (mWebview == null) {
            mWebview = WebView(this)
            mWebview!!.settings.javaScriptEnabled = true
            mWebview!!.setBackgroundColor(applicationContext.resources.getColor(R.color.colorPrimary))
            val settings: WebSettings = mWebview!!.settings
            settings.domStorageEnabled = true
            mWebview!!.webViewClient = CustomWebView(this, this@AudioRecordActivity)

        }
        record_audio.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkRequiredPermissionsOnce()
            } else {
                startRecording()
            }
        }

        webview.setOnClickListener {
            showWebView("slider")
        }

        refresh.setOnClickListener {
          try {
              stopRecording()
              startRecording()
          }
          catch (e:Exception){
              e.printStackTrace()
          }
        }

        done.setOnClickListener {

            onUpload()
        }

        cancel.setOnClickListener {

            stopRecording()
            audiolayout.visibility = View.VISIBLE
            recordinglayout.visibility = View.GONE
            layout_firebase_result.visibility = View.GONE

        }
        img_firebase_result_slider.setOnClickListener {
            showWebView("slider")

        }
        img_firebase_result_mic.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkRequiredPermissionsOnce()
            } else {
                startRecording()
            }
        }
        layout_menu.setOnClickListener {
            showDialog(this)
        }
        layout_back.setOnClickListener {
            layout_audio_record.visibility = View.VISIBLE
            mainLayout.setBackgroundResource(R.drawable.bg)

            layout_frames.visibility = View.GONE
            layout_web_text.visibility = View.GONE
        }
        layout_audio_record.visibility = View.VISIBLE

        refresh.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        refresh.setImageDrawable(resources.getDrawable(R.drawable.refresh_white))
                    }
                    MotionEvent.ACTION_UP -> {
                        refresh.setImageDrawable(resources.getDrawable(R.drawable.refresh))
                    }
                }
                return false
            }
        })
    }

    private fun initView() {

    }


    private fun startRecording() {

        val database = FirebaseDatabase.getInstance(Constants.FIREBASE_DB_URL)
        val ref = database.getReference("messages")
        if (TextUtils.isEmpty(userid))
            userid = SharedPreference.getSharedPreferenceString(this, Constants.SharedPreference.USERID, "").toString()
        var query = ref!!.child(userid)
        query.removeValue()

        ApiManager(this, this).createCloudCustomToken()
        audiolayout.visibility = View.GONE
        recordinglayout.visibility = View.VISIBLE
        layout_firebase_result.visibility = View.GONE
        done.visibility = View.GONE
        refresh.visibility = View.VISIBLE
        text1.visibility = View.VISIBLE
        text2.visibility = View.GONE

        fileName = "${externalCacheDir!!.absolutePath}/${UUID.randomUUID()}.wav"
        Log.e("filename", fileName)

        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setMaxDuration(10000)
            setOutputFormat(MediaRecorder.OutputFormat.AMR_WB)
            setAudioSamplingRate(16000)
            setOutputFile(fileName)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB)

            try {
                prepare()
            } catch (e: IOException) {
                Log.e("Audio record", "prepare() failed")
            }

            circularProgressBar.progress = 0f
            setProgressWithAnimation(applicationContext, 0F, 10000)
            circularProgressBar.setProgressWithAnimation(100f, 10000)

            start()
        }

        layout_ripplepulse.startRippleAnimation()

        timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    stopRecording()
                    text1.visibility = View.GONE
                    text2.visibility = View.VISIBLE
                    refresh.visibility = View.GONE
                    done.visibility = View.VISIBLE
                }
            }
        }, 10000)

    }

    private fun onUpload() {
        if (!Network.isConnected(applicationContext)) {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show()
            return
        }

        showProgressDialog(true)

        val storage = Firebase.storage("gs://joye-768f7.appspot.com")

        val storageRef = storage.reference

        var file = Uri.fromFile(File(fileName))
        val riversRef = storageRef.child("audio/${file.lastPathSegment}")
        var metadata = storageMetadata {
            contentType = "application/octet-stream"
        }
        var uploadTask = riversRef.putFile(file, metadata)

        uploadTask.addOnFailureListener {
            showProgressDialog(false)
            Toast.makeText(this, "Error in uploading audio, Please try again!!", Toast.LENGTH_SHORT).show()
            Log.e("exception", it.toString())

        }.addOnSuccessListener {
            Log.e("sucess", it.toString())

            val filename: String = fileName!!.substring(fileName!!.lastIndexOf("/") + 1)
            var jsonobject1 = JSONObject()
            jsonobject1.put("enableAutomaticPunctuation", true)
            jsonobject1.put("encoding", "AMR_WB")
            jsonobject1.put("sampleRateHertz", 16000)
            jsonobject1.put("languageCode", "en-US")
            jsonobject1.put("model", "default")
            var jsoobject2 = JSONObject()
            jsoobject2.put("uri", Constants.google_cloud_url + filename)
            var jsonObject = JSONObject()
            jsonObject.put("config", jsonobject1)
            jsonObject.put("audio", jsoobject2)


            var requestBody = RequestBody.create("text/html; charset=UTF-8".toMediaTypeOrNull(), jsonObject.toString())

            ApiManager(this, this).convertSpeechToText(requestBody)

        }

    }

    private fun stopRecording() {
        if (timer != null) {
            timer.cancel()
        }
        layout_ripplepulse.stopRippleAnimation()
      try {
          recorder?.apply {
              stop()
              release()
          }
      }catch (e:Exception){
          e.printStackTrace()
      }
        recorder = null
        circularProgressBar.progress = 0F

    }

    override fun onStop() {
        super.onStop()

        recorder?.release()
        recorder = null
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkRequiredPermissionsOnce() {
        val permissions: Array<String> = arrayOf(Manifest.permission.RECORD_AUDIO)
        val requestpermiosn = java.util.ArrayList<String>()
        for (s in permissions) {
            if (!hasPermission(s)) {
                requestpermiosn.add(s)
            }
        }

        if (requestpermiosn.size > 0) {
            var stockArr = arrayOfNulls<String>(requestpermiosn.size)
            stockArr = requestpermiosn.toArray(stockArr)

            requestPermissions(stockArr, MY_PERMISSIONS_REQUESINT)

        } else {
            startRecording()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUESINT ->
                if (grantResults.size > 0) {

                    for (i in 0..grantResults.size - 1) {

                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                            state = true
                        } else {

                            state = false
                            break
                        }
                    }
                    if (state == true) {
                        startRecording()
                    } else {
                        Toast.makeText(this, "allow necessary permission", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

    private fun hasPermission(permission: String): Boolean {

        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || this.checkSelfPermission(permission) === PackageManager.PERMISSION_GRANTED
    }

    override fun getToken(token: String) {
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun showDialog(activity: AudioRecordActivity?) {
        var customdialog = Dialog(activity!!, R.style.D1NoTitleDim)
        customdialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customdialog!!.setContentView(R.layout.layout_dialog)
        customdialog!!.setCanceledOnTouchOutside(true)
        var lp = customdialog.window!!.attributes
        lp.dimAmount = 0.0f
        lp.gravity = Gravity.TOP
        customdialog!!.window!!.attributes = lp
        customdialog!!.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup
                .LayoutParams.WRAP_CONTENT)
        customdialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH)


        var btn_faq = customdialog!!.findViewById<AppCompatButton>(R.id.btn_faq)
        var btn_tnc = customdialog!!.findViewById<AppCompatButton>(R.id.btn_tnc)
        var btn_privacy = customdialog!!.findViewById<AppCompatButton>(R.id.btn_privacy)
        var view_dialog = customdialog!!.findViewById<LinearLayout>(R.id.view_dialog)

        btn_faq.setOnClickListener {
            getDataFromFirebase("android_q&a_html")
            customdialog!!.dismiss()
        }
        btn_tnc.setOnClickListener {
            getDataFromFirebase("android_tos_html")
            customdialog!!.dismiss()

        }
        btn_privacy.setOnClickListener {
            getDataFromFirebase("android_privacy_policy_html")

            customdialog!!.dismiss()

        }
        view_dialog.setOnTouchListener { _, _ ->
            customdialog!!.dismiss()
            return@setOnTouchListener true
        }
        customdialog!!.show()
    }

    /*
   * get data from firebase
   * */
    private fun getDataFromFirebase(page: String) {
        privacy_url = ""
        faq_url = ""
        tnc_url = ""
        showProgressDialog(true)
        val database = FirebaseDatabase.getInstance(Constants.FIREBASE_DB_URL)
        val ref = database.getReference("joye_master_data")
        var query = ref!!.child("joye_docs").child(page)
        query!!.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                showProgressDialog(false)
                Toast.makeText(applicationContext,getString(R.string.txt_app_error_msg),Toast.LENGTH_SHORT).show()

            }

            override fun onDataChange(snapshot: DataSnapshot) {

                when {
                    snapshot.key!! == "android_privacy_policy_html" -> {

                        privacy_url = snapshot!!.getValue(String::class.java).toString()
                        setFramesData(privacy_url)
                    }
                    page == "android_q&a_html" -> {
                        faq_url = snapshot!!.getValue(String::class.java).toString()
                        setFramesData(faq_url)
                    }
                    page == "android_tos_html" -> {

                        tnc_url = snapshot!!.getValue(String::class.java).toString()
                        setFramesData(tnc_url)
                    }
                }

            }
        })
    }

    private fun setFramesData(url: String) {
        Log.e("url", url)
        layout_audio_record.visibility = View.GONE
        layout_frames.visibility = View.VISIBLE
        mainLayout.setBackgroundColor(R.color.colorPrimary)

        val settings: WebSettings = web_frames!!.settings
        settings.javaScriptEnabled = true
        settings.textZoom = 25
        settings.domStorageEnabled = true
        web_frames!!.webViewClient = CustomWebView1(this, this@AudioRecordActivity)

        web_frames.loadUrl(url)


    }

    override fun onDownloadStart(url: String?, userAgent: String?, contentDisposition: String?, mimeType: String?, contentLength: Long) {

    }

    override fun getTextFromSpeech(response: SpeechToText) {

        var text = response.results.get(0).alternatives[0].transcript
        Log.e("text", text)
        val database = FirebaseDatabase.getInstance(Constants.FIREBASE_DB_URL)
        userid = SharedPreference.getSharedPreferenceString(this, Constants.SharedPreference.USERID, "").toString()

        val tsLong = System.currentTimeMillis() / 1000
        val timestamp = tsLong.toString()
        Log.e("timestamp", timestamp)
        var list = ArrayList<String>()
        list.add("empty")

        val ref = userid?.let { database.getReference("messages").child(it).child(timestamp) }
        var datamodal = DataModal()
        datamodal.emotion = list
        datamodal.situation = list
        var textmodal = TextModal()
        textmodal.data = datamodal
        textmodal.failed = false
        textmodal.from = "user"
        textmodal.layout = ""
        textmodal.message_read = "N"
        textmodal.resolved = false
        textmodal.text = text
        textmodal.timestamp = timestamp.toLong()
        textmodal.type = "text"

        if (ref != null) {
            ref.setValue(textmodal) { error, ref ->
                Log.e("ref", ref.toString())
                isFirstCall = false
                getDataFromFirebase()
            }
        }
    }

    override fun getTextFromSpeechError() {

        audiolayout.visibility = View.GONE
        recordinglayout.visibility = View.GONE
        layout_firebase_result.visibility = View.VISIBLE
    }

    override fun getOtpVerifySuccess() {
        TODO("Not yet implemented")
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (mWebview!!.canGoBack()) {
                        mWebview!!.goBack()

                    } else {
                        finishAffinity();
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }


    @SuppressLint("NewApi")

    override fun onResume() {
        super.onResume()

    }

    @SuppressLint("NewApi")
    override fun onPause() {
        super.onPause()
    }

    @Override
    override fun onDestroy() {
        super.onDestroy()
    }

    /*
     * get data from firebase
     * */
    private fun getDataFromFirebase() {

        val database = FirebaseDatabase.getInstance(Constants.FIREBASE_DB_URL)
        val ref = database.getReference("messages")
        if (TextUtils.isEmpty(userid))
            userid = SharedPreference.getSharedPreferenceString(this, Constants.SharedPreference.USERID, "").toString()

        var query = ref!!.child(userid)


        query!!.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("getDataFromFirebase", "failure")
                showProgressDialog(false)
                Toast.makeText(applicationContext,getString(R.string.txt_app_error_msg),Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {

                Log.e("ondatachange", "ondatachange")

                if (!firebaseTextList.isNullOrEmpty())
                    firebaseTextList!!.clear()

                for (ds in snapshot.children) {
                    if (firebaseTextList.isNullOrEmpty())
                        firebaseTextList = ArrayList()
                    if (ds.child("is_last_message").getValue(String::class.java).equals("true")) {
                        var recieveTextModal: RecieveTextModal = RecieveTextModal()
                        var is_abusive = ds.child("is_abusive").getValue(String::class.java)
                        var is_last_message = ds.child("is_last_message").getValue(String::class.java)
                        var show_pie_graph_url = ds.child("show_pie_graph_url").getValue(String::class.java)
                        var delay = ds.child("delay").getValue(Any::class.java)
                        var data = ds.child("data").getValue(DataModal::class.java)
                        var failed = ds.child("failed").getValue(Boolean::class.java)
                        var layout = ds.child("layout").getValue(String::class.java)
                        var from = ds.child("from").getValue(String::class.java)
                        var message_read = ds.child("message_read").getValue(String::class.java)
                        var resolved = ds.child("resolved").getValue(Boolean::class.java)
                        var text = ds.child("text").getValue(String::class.java)
                        var timestamp = ds.child("timestamp").getValue(Long::class.java)
                        var type = ds.child("type").getValue(String::class.java)
                        if (data != null) {
                            recieveTextModal.data = data
                        }
                        if (is_abusive != null) {
                            recieveTextModal.is_abusive = is_abusive
                        }
                        if (is_last_message != null) {
                            recieveTextModal.is_last_message = is_last_message
                        }
                        recieveTextModal.show_pie_graph_url = show_pie_graph_url
                        recieveTextModal.delay = delay
                        if (failed != null) {
                            recieveTextModal.failed = failed
                        }
                        recieveTextModal.from = from
                        recieveTextModal.layout = layout
                        recieveTextModal.message_read = message_read
                        if (resolved != null) {
                            recieveTextModal.resolved = resolved
                        }
                        recieveTextModal.text = text
                        if (timestamp != null) {
                            recieveTextModal.timestamp = timestamp
                        }
                        recieveTextModal.type = type
                        firebaseTextList!!.add(recieveTextModal)
                    }
                }
                if(!isFirstCall)
                setDataAccToFirebaseData()
            }
        })
    }

    private fun setDataAccToFirebaseData() {
        // get last index modal
        if (!firebaseTextList.isNullOrEmpty()) {
            var lastModal = firebaseTextList!!.get(firebaseTextList!!.size - 1)
            Log.e("TAG", "$lastModal")

            if (lastModal != null) {
                if (lastModal.is_last_message == "true") {
                    isFirstCall= true
                    if (lastModal.is_abusive == "true") {
                        showProgressDialog(false)
                        showCautionDialog(this)
                    } else if (lastModal.show_pie_graph_url == "true") {
                       showWebView("piechart")

                    } else {
                        showProgressDialog(false)
                        audiolayout.visibility = View.GONE
                        recordinglayout.visibility = View.GONE
                        layout_firebase_result.visibility = View.VISIBLE
                    }
                } else {
                    showProgressDialog(false)
                }

            }
        } else {
            //viewModal.showStringSnackbarMessage(getString(R.string.txt_app_error_msg))
        }
    }

    private fun showCautionDialog(activity: Context) {
        if (!(activity as Activity).isFinishing) {

            var dialog = Dialog(activity!!, R.style.D1NoTitleDimNoAnimation)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            var lp = dialog!!.window!!.attributes;
            lp.dimAmount = 0.0f;
            dialog!!.window!!.attributes = lp
            dialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND)
            dialog!!.setCancelable(true)
            dialog!!.setContentView(R.layout.layout_caution_dialog)

            var btn_ok = dialog!!.findViewById<AppCompatButton>(R.id.btn_ok)

            btn_ok.setOnClickListener {
                audiolayout.visibility = View.VISIBLE
                recordinglayout.visibility = View.GONE
                layout_firebase_result.visibility = View.GONE
                dialog.dismiss()
            }

            dialog!!.show()

        }
    }

    private fun showWebView(action: String) {
        if (!Network.isConnected(applicationContext)) {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show()
            return
        }

        if (action != "piechart")
            showProgressDialog(true)

        token = SharedPreference.getSharedPreferenceString(this, Constants.SharedPreference.WEBVIEWTOKEN, "")
        Log.e("token", token)
        if (!mWebview!!.isVisible)
            mWebview!!.visibility = View.VISIBLE

        if (token != null) {

            mWebview!!.loadUrl(Constants.URL+"m-login/${token}/${action}")

        } else {

            Toast.makeText(applicationContext,"Token is null",Toast.LENGTH_SHORT).show()

        }
    }

    private fun callScreen() {
        val nextScreenIntent = Intent(this, AudioRecordActivity::class.java)
        startActivity(nextScreenIntent)
        overridePendingTransition(R.anim.fade_out, R.anim.fade_in)
        finish()
    }

    class CustomWebView1(var context: Context, var activity: AudioRecordActivity) : WebViewClient
    () {
        override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {

        }

        @TargetApi(Build.VERSION_CODES.M)
        override fun onReceivedError(view: WebView, req: WebResourceRequest, rerr: WebResourceError) {
            Toast.makeText(activity,activity.getString(R.string.txt_app_error_msg),Toast.LENGTH_SHORT).show()

            activity.showProgressDialog(false)
            Log.e("onPageError", "" + rerr)
        }


        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return if (url!!.startsWith("tel:") || url!!.startsWith("sms:") || url!!.startsWith("smsto:") || url!!
                            .startsWith("mailto:") || url!!.startsWith("mms:") || url!!.startsWith("mmsto:")) {
                var intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                activity.startActivity(intent)
                true
            } else {
                view!!.loadUrl(url)
                true
            }
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            if(activity.dialog!=null && !activity.dialog!!.isShowing)
            activity.showProgressDialog(true)


        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            Log.e("onpagefinished", "onpagefinished")
            activity.showProgressDialog(false)
            activity.layout_web_text.visibility = View.VISIBLE


        }


    }

    class CustomWebView(var context: Context, var activity: AudioRecordActivity) : WebViewClient() {
        override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {

        }

        @TargetApi(Build.VERSION_CODES.M)
        override fun onReceivedError(view: WebView, req: WebResourceRequest, rerr: WebResourceError) {
            // Redirect to deprecated method, so you can use it in all SDK versions
            onReceivedError(view, rerr.errorCode, rerr.description.toString(), req.url.toString())
        }


        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            var changeUrl = view!!.url

            return true
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)

        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            Log.e("onpagefinished", "onpagefinished")
            activity.showProgressDialog(false)

            activity.setContentView(activity.mWebview)

        }

        override fun doUpdateVisitedHistory(view: WebView?, url: String?, isReload: Boolean) {
            super.doUpdateVisitedHistory(view, url, isReload)
            if (url == Constants.URL+"end-brew") {
                if (activity.mWebview != null) {
                    activity.callScreen()
                }
            }

        }
    }

    /**
     * Set the progress with an animation.
     * Note that the {@link ObjectAnimator} Class automatically set the progress
     * so don't call the {@link CircularProgressBar#setProgress(float)} directly within this method.
     *
     * @param progress The progress it should animate to it.
     * @param duration The length of the animation, in milliseconds.
     */
    @SuppressLint("ObjectAnimatorBinding")
    fun setProgressWithAnimation(context: Context, progress: Float, duration: Long) {
        setAnimationAsDefault(context)
        var objectAnimator = ObjectAnimator.ofFloat(this, "progress", progress)
        objectAnimator.duration = duration
        objectAnimator.interpolator = LinearInterpolator()
        objectAnimator.start()
    }

    private fun setAnimationAsDefault(context: Context) {
        var durationScale = Settings.Global.getFloat(context.contentResolver,
                Settings.Global.ANIMATOR_DURATION_SCALE, 0F)
        if (durationScale != 1F) {
            try {
                ValueAnimator::class.java.getMethod("setDurationScale", Float::class.java).invoke(null, 1f)
                durationScale = 1f
            } catch (t: Throwable) {
             }
        }
    }

}