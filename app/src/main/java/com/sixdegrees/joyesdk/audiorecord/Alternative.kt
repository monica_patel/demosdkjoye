package com.sixdegrees.joyesdk.audiorecord

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class Alternative (
    @Json(name = "transcript")
    var transcript: String,
    @Json(name = "confidence")
    var confidence: String

)