package com.sixdegrees.joyesdk.audiorecord

import android.os.Build
import android.os.FileObserver
import android.util.Log
import androidx.annotation.RequiresApi
import java.io.File


@RequiresApi(Build.VERSION_CODES.Q)
class MyFileObserver(path: File?, mask: Int) : FileObserver(path!!, mask) {
    override fun onEvent(event: Int, path: String?) {
        Log.e("finish record","finish record")
        // start playing
    }
}