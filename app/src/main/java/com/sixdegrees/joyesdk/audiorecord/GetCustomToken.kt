package com.sixdegrees.joyesdk.audiorecord


interface GetCustomToken {

    fun getToken(token:String)
    fun getTextFromSpeech(response: SpeechToText)
    fun getTextFromSpeechError()
    fun getOtpVerifySuccess()
}