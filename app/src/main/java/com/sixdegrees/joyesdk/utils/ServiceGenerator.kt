package com.sixdegrees.joyesdk.utils

import android.content.Context
import com.joye.sixdegreesit.BuildConfig


import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by AhmedEltaher
 */

@Singleton
class ServiceGenerator @Inject
constructor() {

    //Network constants
    private val timeoutConnect = 30   //In seconds
    private val timeoutRead = 30   //In seconds
    private val contentType = "Content-Type"
    private val contentTypeValue = "text/plain"
    private var okHttpClient: OkHttpClient? = null

    private val okHttpBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
    private var retrofit: Retrofit? = null

    private var headerInterceptor = Interceptor { chain ->
        val original = chain.request()

        val request = original.newBuilder()
                .header(contentType, contentTypeValue)
                .method(original.method, original.body)
                .build()

        chain.proceed(request)
    }

    private val logger: HttpLoggingInterceptor
        get() {
            val loggingInterceptor = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                loggingInterceptor.apply { loggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS }.level = HttpLoggingInterceptor.Level.BODY
            }
            return loggingInterceptor
        }

//    init {
//        okHttpBuilder.addInterceptor(headerInterceptor)
//        okHttpBuilder.addInterceptor(logger)
//        okHttpBuilder.connectTimeout(timeoutConnect.toLong(), TimeUnit.SECONDS)
//        okHttpBuilder.readTimeout(timeoutRead.toLong(), TimeUnit.SECONDS)
//        val client = okHttpBuilder.build()
//        retrofit = Retrofit.Builder()
//                .baseUrl(customTokenUrl).client(client)
//                .addConverterFactory(MoshiConverterFactory.create())
//                .build()
//    }

    fun <S> createService(serviceClass: Class<S>, baseUrl: String,context: Context): S {
        if (okHttpClient == null) initOkHttp(context)
//        val client = okHttpBuilder.build()
       
        retrofit = Retrofit.Builder()
                .baseUrl(baseUrl).client(okHttpClient)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        return retrofit!!.create(serviceClass)
    }

    private fun initOkHttp(context: Context) {

        val httpClient = OkHttpClient().newBuilder()
                .connectTimeout(timeoutConnect.toLong(), TimeUnit.SECONDS)
                .readTimeout(timeoutRead.toLong(), TimeUnit.SECONDS)
                .writeTimeout(timeoutConnect.toLong(), TimeUnit.SECONDS)
        httpClient.addInterceptor(logger)
        httpClient.addInterceptor(headerInterceptor)

        okHttpClient = httpClient.build()

    }
}