package com.sixdegrees.joyesdk.utils

/**
 * Created by AhmedEltaher
 */

class Constants {
    companion object INSTANCE {
        const val SPLASH_DELAY = 3000
        const val BASE_URL = ""

        private const val FIREBASE_DB_URL_LIVE = "https://joye-dev-2018.firebaseio.com/"
        //private const val FIREBASE_DB_URL_DEV = "https://joye-768f7.firebaseio.com/"
        const val FIREBASE_DB_URL = FIREBASE_DB_URL_LIVE

        private const val URL_LIVE = "http://35.199.149.78:3003/"
        //private const val URL_DEV = "http://35.199.149.78:3005/"
        const val URL = URL_LIVE
        const val baseUrl = "https://us-central1-joye-768f7.cloudfunctions.net/"
        const val speechToTextUrl = "https://speech.googleapis.com/v1/speech:recognize/"
        const val google_cloud_url = "gs://joye-768f7.appspot.com/audio/"

        const val token = "ya29.c.KpQB0gdf4dN1Bwnh5D9tU-TQg0IxjTbtp6XeOpdRlsz4zvtxw1PrmMz3Zmuv6T8mUm3JKAayPqfolNB6DLhF2aZ6g_8Sb_ePy-WSVODg8IFsB4SC-7VL1Tk0uIigJoAQ9wDFcBOZDr4TdqAnHTQ8mr3Wwwc0BEBAficMH07ffPmL5MRdkaGptYggRcUo7F8cdSqGiuuvuw"

    }

    class SharedPreference {
        companion object INSTANCE {
            const val USERID = "userid"
            const val IS_MORNING_NOTIFICATION_SET = "morning_notification_set"
            const val FCM_PARAM = "fcm"
            const val CLOUD_TOKEN = "cloud_token"
            const val FIRST_TIME_VISIT = "first_time_visit"
            const val WEBVIEWTOKEN = "web_view_token"
            const val OTP_VERIFIED = "otp_verified"

        }
    }
}