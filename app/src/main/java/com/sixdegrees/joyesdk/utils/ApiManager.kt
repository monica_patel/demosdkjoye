package com.sixdegrees.joyesdk.utils

import android.util.Log
import android.widget.Toast
import com.joye.sixdegreesit.R
import com.sixdegrees.joyesdk.BaseActivity

import com.sixdegrees.joyesdk.audiorecord.CloudToken

import com.sixdegrees.joyesdk.audiorecord.GetCustomToken
import com.sixdegrees.joyesdk.audiorecord.SpeechToText
import com.sixdegrees.joyesdk.audiorecord.VerificationCode
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiManager(var context: BaseActivity, var customToken: GetCustomToken) {
    var serviceGenerator: ServiceGenerator = ServiceGenerator()

    fun createCustomToken(uid:String) {
        val map: MutableMap<String, String> = HashMap()
        map["uid"] = uid
        val baseServiceList = serviceGenerator.createService(
            ServiceList::class.java,
            Constants.baseUrl,context)
        val call = baseServiceList.createCustomToken(map)
        call.enqueue(object : Callback<String?> {
            override fun onResponse(call: Call<String?>?, response: Response<String?>) {
                if (response.isSuccessful) {
                    context.showProgressDialog(false)
                    Log.e("response", response.toString())
                    if (response.body() != null) {

                        customToken.getToken(response.body().toString())

                    }

                }
            }

            override fun onFailure(call: Call<String?>?, t: Throwable?) { //Handle failure
                Log.e("error", call.toString())
                context.showProgressDialog(false)


            }

        })
    }

    fun convertSpeechToText(jsonObject:RequestBody){
        if (!Network.isConnected(context)) {
            Toast.makeText(context, R.string.no_internet,Toast.LENGTH_SHORT).show()
            return
        }
        var cloudtoken = SharedPreference.getSharedPreferenceString(context,
            Constants.SharedPreference.CLOUD_TOKEN,"")
        val baseServiceList = serviceGenerator.createService(ServiceList::class.java,
            Constants.speechToTextUrl,context)
        val call = baseServiceList.speechToText(token = "Bearer ${cloudtoken}",jsonObject=jsonObject)
        call.enqueue(object : Callback<SpeechToText?> {
            override fun onResponse(call: Call<SpeechToText?>?, response: Response<SpeechToText?>) {
                Log.e("response", response.toString())
                if (response.isSuccessful) {
                    Log.e("response", response.toString())
                    if (response.body() != null) {

                        customToken.getTextFromSpeech(response.body()!!)

                    }

                }
            }

            override fun onFailure(call: Call<SpeechToText?>?, t: Throwable?) { //Handle failure
                Log.e("error", call.toString())
                customToken.getTextFromSpeechError()
                context.showProgressDialog(false)
            }

        })
    }

    fun createCloudCustomToken() {
        val map: MutableMap<String, String> = HashMap()
        val baseServiceList = serviceGenerator.createService(ServiceList::class.java,
            Constants.baseUrl,context)
        val call = baseServiceList.createCloudCustomToken(map)
        call.enqueue(object : Callback<CloudToken?> {
            override fun onResponse(call: Call<CloudToken?>?, response: Response<CloudToken?>) {
                if (response.isSuccessful) {
                    Log.e("response", response.toString())
                    if (response.body() != null) {


                        SharedPreference.setSharedPreferenceString(context,
                            Constants.SharedPreference.CLOUD_TOKEN,response.body()!!.accessToken)

                    }

                }
            }

            override fun onFailure(call: Call<CloudToken?>?, t: Throwable?) { //Handle failure
                Log.e("error", call.toString())

            }

        })
    }

    fun getVerificationCode(map:Map<String,String>){

//        context.showProgressDialog(true)

        val baseServiceList = serviceGenerator.createService(ServiceList::class.java,
            Constants.baseUrl,context)
        val call = baseServiceList.getVerificationCode(map)
        call.enqueue(object : Callback<VerificationCode?> {
            override fun onResponse(call: Call<VerificationCode?>?, response: Response<VerificationCode?>) {
                if (response.isSuccessful) {
                    Log.e("response", response.toString())
                    if (response.body() != null) {

                        customToken.getOtpVerifySuccess()
                        Toast.makeText(context,"Otp sent successfully",Toast.LENGTH_SHORT).show()

                    }

                }
            }

            override fun onFailure(call: Call<VerificationCode?>?, t: Throwable?) { //Handle failure
                Log.e("error", call.toString())

                context.showProgressDialog(false)
            }

        })
    }

}