package com.sixdegrees.joyesdk.utils


import com.sixdegrees.joyesdk.audiorecord.CloudToken
import com.sixdegrees.joyesdk.audiorecord.SpeechToText
import com.sixdegrees.joyesdk.audiorecord.VerificationCode
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.http.*


/**
 * Created by AhmedEltaher
 */

interface ServiceList {

    @FormUrlEncoded
    @POST("createCustomTokenTest/")
    fun createCustomToken(@FieldMap map: Map<String, String>) :retrofit2.Call<String>

    @FormUrlEncoded
    @POST("getGoogleApiToken/")
    fun createCloudCustomToken(@FieldMap map: Map<String, String>) :retrofit2.Call<CloudToken>

    @FormUrlEncoded
    @POST("getVerificationCode/")
    fun getVerificationCode(@FieldMap map: Map<String, String>) :retrofit2.Call<VerificationCode>

    @POST(".")
    fun speechToText(@Header("Authorization") token:String,@Body jsonObject: RequestBody) :retrofit2.Call<SpeechToText>

}