package com.sixdegrees.joyesdk.utils.font

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet

class CustomAvinerHeavyButton: androidx.appcompat.widget.AppCompatButton {
    constructor(context: Context) : super(context) {

        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {

        init()
    }

    override fun setText(text: CharSequence, type: BufferType?) {
        var text = text
        if (text.length > 0) {
            text = text[0].toString().toUpperCase() + text.subSequence(1, text.length)
        }
        super.setText(text, type)
    }

    open fun init() {
        val otfName = "fonts/avenirblack.ttf"
        val font = Typeface.createFromAsset(context!!.assets, otfName)
        this.typeface = font
    }
}