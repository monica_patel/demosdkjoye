package com.sixdegrees.joyesdk.utils

import android.app.*
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val TAG = "MyFirebaseMessagingService======="
    private lateinit var notificationManager: NotificationManager
    lateinit var ADMIN_CHANNEL_ID: String
    override fun onNewToken(token: String) {
        super.onNewToken(token)
        SharedPreference.setSharedPreferenceString(
            this,
            Constants.SharedPreference.FCM_PARAM,
            token
        )

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)


    }

//    @RequiresApi(api = Build.VERSION_CODES.O)
//    private fun setupNotificationChannels() {
//        val adminChannelName = getString(R.string.notifications_admin_channel_name)
//        val adminChannelDescription = getString(R.string.notifications_admin_channel_description)
//
//        val adminChannel: NotificationChannel
//        adminChannel = NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH)
//        adminChannel.description = adminChannelDescription
//        adminChannel.enableLights(true)
//        adminChannel.lightColor = Color.RED
//        adminChannel.enableVibration(true)
//        notificationManager.createNotificationChannel(adminChannel)
//    }
//
//    private fun isForeground(context: Context): Boolean {
//        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
//        val runningTaskInfo = manager.getRunningTasks(1)
//        val componentInfo = runningTaskInfo[0].topActivity
//        return componentInfo!!.packageName == this.packageName
//    }
}