package com.task.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet



class Circle(context: Context?, attrs: AttributeSet?) : androidx.appcompat.widget.AppCompatImageView(
    context!!, attrs) {
    private val paint: Paint
    private val rect: RectF
    var angle: Float

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawArc(rect, START_ANGLE_POINT, angle, false, paint)
    }


    companion object {
        private const val START_ANGLE_POINT = 260f
    }

    init {
        val strokeWidth = 30f
        paint = Paint()
        paint.setAntiAlias(true)
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = strokeWidth
        //Circle color
        paint.setColor(Color.WHITE)

        //size 200x200 example
        rect = RectF(strokeWidth.toFloat(), strokeWidth.toFloat(), (230 + strokeWidth).toFloat(), (230 + strokeWidth).toFloat())

        //Initial Angle (optional, it can be zero)
        angle = 0f
    }
}